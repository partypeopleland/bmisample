﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BMI.Models;

namespace BMI.Controllers.Tests {
    [TestClass()]
    public class BMIControllerTests {
        [TestMethod()]
        public void CalcutedBMI() {
            Person person = new Person() { KG = 41, CM = 153 };
            BMICal target = new BMICal();
            double actual = target.CalcutedBMI(person);
            double expected = 17.51;
            Assert.AreEqual(expected, actual);
        }
    }
}