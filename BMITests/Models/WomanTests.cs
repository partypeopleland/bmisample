﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMI.Controllers.Tests {
    [TestClass()]
    public class WomanTests {
        [TestMethod()]
        public void ShowResultTest_Woman_03() {
            double BMI = 17.51;
            Woman target = new Woman(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "太瘦";
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void ShowResultTest_Woman_02() {
            double BMI = 18;
            Woman target = new Woman(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "適中";
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void ShowResultTest_Woman_01() {
            double BMI = 22;
            Woman target = new Woman(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "太胖";
            Assert.AreEqual(expected, actual);
        }
    }
}