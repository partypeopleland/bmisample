﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMI.Controllers.Tests {
    [TestClass()]
    public class ManTests {
        [TestMethod()]
        public void ShowResultTest_Man_03() {
            double BMI = 17.51;
            Man target = new Man(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "太瘦";
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void ShowResultTest_Man_02() {
            double BMI = 20;
            Man target = new Man(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "適中";
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void ShowResultTest_Man_01() {
            double BMI = 25;
            Man target = new Man(BMI);
            target.ShowResult();
            string actual = target.result;
            string expected = "太胖";
            Assert.AreEqual(expected, actual);
        }
    }
}