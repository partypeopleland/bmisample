﻿using System;
using BMI.Models;

namespace BMI.Controllers {
    public class Man : IShowBMIResult {
        private double _BMI;
        public Man(double BMI) {
            _BMI = BMI;
        }
        private string _result;
        public string result { get { return _result; } }

        public string ShowResult() {
            if (_BMI < 20) _result = "太瘦";
            if (_BMI >= 25) _result = "太胖";
            if (20 <= _BMI && _BMI < 25) _result = "適中";
            return _result;
        }
    }
}