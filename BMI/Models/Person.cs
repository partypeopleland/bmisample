﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMI.Controllers;

namespace BMI.Models {
    public class Person {
        [Display(Name = "身高")]
        public int CM { get; set; }
        [Display(Name = "體重")]
        public int KG { get; set; }
    }

    public class BMIResult {
        public Person person { get; set; }
        public IShowBMIResult man { get; set; }
        public IShowBMIResult woman { get; set; }
        public double BMI { get; set; }
    }

    public interface IShowBMIResult {
        string ShowResult();
    }
}
