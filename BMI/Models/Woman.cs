﻿using System;
using BMI.Models;

namespace BMI.Controllers {
    public class Woman : IShowBMIResult {
        private double _BMI;
        public Woman(double BMI) {
            _BMI = BMI;
        }
        private string _result;
        public string result { get { return _result; } }
        public string ShowResult() {
            if (_BMI < 18) _result = "太瘦";
            if (_BMI >= 22) _result = "太胖";
            if (18 <= _BMI && _BMI < 22) _result = "適中";
            return _result;
        }
    }
}