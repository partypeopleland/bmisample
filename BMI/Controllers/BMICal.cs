﻿using System;
using BMI.Models;

namespace BMI.Controllers {
    public class BMICal {
        public BMICal() {
        }

        public double CalcutedBMI(Person person) {
            double KG = person.KG;
            double M = (double)person.CM / 100;
            double BMIValue = Math.Round(KG / (M * M), 2);
            return BMIValue;
        }
    }
}