﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BMI.Models;

namespace BMI.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Person person) {
            //驗證輸入資料
            if (person == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //計算BMI
            double BMIValue = new BMICal().CalcutedBMI(person);

            //計算男生結果
            IShowBMIResult man = new Man(BMIValue);
            man.ShowResult();

            //計算女生結果
            IShowBMIResult woman = new Woman(BMIValue);
            woman.ShowResult();

            //輸出結果
            BMIResult result = CreateResult(person, BMIValue, man, woman);
            return View(result);

        }

        /// <summary>
        /// 計算BMI
        /// </summary>
        /// <param name="person"></param>
        /// <param name="BMIValue"></param>
        /// <param name="man"></param>
        /// <param name="woman"></param>
        /// <returns></returns>
        private static BMIResult CreateResult(Person person, double BMIValue, IShowBMIResult man, IShowBMIResult woman) {
            return new BMIResult {
                BMI = BMIValue,
                man = man,
                woman = woman,
                person = person
            };
        }
    }
}